# Glowing Succotash

(project name auto-generated 🤷)

## What's this about ?

Small project to

- Get the top 20 articles from Hacker News and surface as JSON `deploy/`
- Create a front-end to consume said service `front-end/`
- A quick kubernetes based deployment of above service + front end `/deploy`

## Dependencies

- Minikube w/ libVirt ( could easily work in kind, k3s or any other k8s distro)
- Docker

## Improvements

- [ ] Get CI in place
- [ ] Add semver to build pipelines and artefact outputs
- [ ] Get linting in place
- [ ] Add in unit tests
- [ ] Add in simple integration test using hurl
- [ ] Write Terraform to spin up a Kubernetes cluster in AWS or GCP
- [ ] Deploy to GKE or EKS
- [ ] Use an express or similar web framework with security modules to harden application and allow for more sustainable development
- [ ] cache
  - [ ] Add a local redis cache to cache results at the backend and serve-while-stale
  - [ ] _or_ Add a Varnish cache in front with serve-while-stale
- [ ] Add a reverse proxy to handle security concerns (nginx + modsecurity)

---

# Docs

## Deployment into (local) Kubernetes

Using Kustomize (because I've never used it before) on Minikube and if I get time onto an extant GKE cluster and if more time then onto EKS using Terraform to provision the cluster.

```bash
# spin up a minkube cluster, assuming you have mainline minikube installed
(cd deploy && ./create-k8s-minikube-cluster.sh)

# ensure you have a localhosts override for succotash.info
# that's the DNS name we'll map to our ingress controller
echo "$(minikube ip) api.succotash.local succotash.local" | sudo tee -a /etc/hosts

# build and deploy containers, using Kustomize to patch the k8s manifests
(cd deploy && ./deploy.sh)
```

## Local Development of components

We're using docker-compose to work on the application locally.  We have two major components:
- front-end on port `8000`
- backend on port `8001`

```bash
# getting started locally
docker-compose up

# OPTIONAL use the `reload` package which auto-refreshes your browser on change using websockets on 8080
# `npm install -g reload`
(cd front-end && reload -e "html|js|css|json|yml")
```

## Hacker news web page in `front-end/`

Consume our Hacker-news API service and display the top 20 articles on a web page.
Application logic is in `main.js`, using Twitter Bootstrap as css framework.

### Improvements

- [ ] Create a web page transpilation using webpack

## Hacker news service in `service/`

Get the top 20 Articles from Hacker News and wrap it in an API response that can be consumed by a front-end

### API Docs

#### GET healthcheck

`GET /healthcheck`

```bash
curl http://localhost:8080/healthcheck
```

**returns**

```json
{ "status": "ok" }
```

#### GET hacker-news

`GET /hacker-news`
Get the top news articles from Hacker News

```bash
curl http://localhost:8080/hacker-news
```

**returns**

```json
{
  "value": {
    "by": "xrayarx",
    "descendants": 89,
    "id": 34558087,
    "kids": [
      34561480,
      34558663
    ],
    "score": 88,
    "time": 1674918351,
    "title": "Netheads vs. bellheads redux: the strange victory of SIP over the telephone",
    "type": "story",
    "url": "https://www.devever.net/~hl/sip-victory#narrow"
}...
```

## License

[MIT Licence](LICENSE)

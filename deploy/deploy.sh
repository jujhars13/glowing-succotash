#!/bin/bash
# usage:
# ENVIRONMENT=development ./deploy.sh

# default to development deployment
declare -r ENVIRONMENT=${ENVIRONMENT:-"development"}

echo "Building and pushing frontend container"

(cd ../front-end &&\
    docker build -t glowing-succotash-frontend:latest . )
minikube image load glowing-succotash-frontend:latest

echo "Building and pushing api container"

(cd ../service &&\
    docker build -t glowing-succotash-api:latest . )
minikube image load glowing-succotash-api:latest

echo "Deploying to ${ENVIRONMENT}"

(cd k8s && \
    kustomize build "${ENVIRONMENT}" |\
    tee |\
    kubectl apply -f - )
